# First login

1. Create a googlecloud.qwiklabs.com & Login to googlecloud.qwiklabs.com with your company account
  2. If it says, username is already taken, say forgot password, and retrieve the password and then login. 
3. **Login to qwiklabs using company email id** & navigate to corresponding lab & click start lab. After lab is started **login to google cloud console using tmp username & password created by qwiklabs**
4. Login to google cloud console using tmp username & password, INSIDE INCOGNITO WINDOW
5. Change the project by clicking on select project and choose the project which begins with "qwiklabs-gcp-<something>"