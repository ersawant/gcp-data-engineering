## Certification Preparation Study Suggestions
1. Go through slides of Data Engineering course found in Qwiklabs and Labs at least once. Check coursera videos too if needed
2. Read documentation of each product covered in the course
    1. Must read Quickstart & Concepts
    2. Good to read Samples & Tutorials & How to Guides

## During the certification exam
1. Read the **complete** question very carefully
2. Read all the answers very carefully 
3. Don't assume more than whats given in the question
4. Use options to eliminate choices
    1. If you are confused between final two choices after elimination, then read the question again very very carefully