### Datalab troubleshooting
1. If datalab create waiting for more than 5 minutes, press Ctrl + C. And then execute command ```datalab connect <name_of_vm>```
2. ```datalab create <name_of_vm>``` - creates the notebook instance. Once it has been already created, you connect to it by executing, ```datalab connect <name_of_vm>```
3. Execution is finished if the left part of the code cell becomes dark blue